package com.facebook.context;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.faceAutomation.configuration.BaseDriver;

public class Login extends Context {

	WebElement element;

	public Login(WebDriver driver) {
		this.driver = driver;
	}

	public void getTextFieldById(String elementId, String elementKey) {

		element = driver.findElement(By.id(elementId));
		element.sendKeys(elementKey);

	}

	public void clickOnButtonById(String elementId) {

		element = driver.findElement(By.id(elementId));
		element.click();
	    System.out.println("Element "+elementId+" is clicked");
		waitForShow();
	}
}
